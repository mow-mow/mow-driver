use embedded_hal::digital::{self, ErrorType, OutputPin, PinState};
use embedded_hal_async::digital::Wait;
use embedded_io::{
    asynch::{Read, Write},
    Io,
};

use crate::{E22xxxtxxs, Error};

pub trait Types {
    type Serial: Read + Write;
    type In: Wait;
    type Out: OutputPin;
}

pub trait Errors {
    type Serial: embedded_io::Error;
    type In: digital::Error;
    type Out: digital::Error;
}

impl<T: Types> Errors for T {
    type Serial = <T::Serial as Io>::Error;
    type In = <T::In as ErrorType>::Error;
    type Out = <T::Out as ErrorType>::Error;
}

pub trait Mode {
    const MODE: (PinState, PinState);
}

pub trait From: Types {
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self;
}

pub trait Into: Types {
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out>;
}

pub trait SwitchMode: Into + Sized {
    async fn switch_mode<M: Mode + From<Serial = Self::Serial, In = Self::In, Out = Self::Out>>(
        self,
    ) -> Result<M, Error<<Self as Errors>::Serial, <Self as Errors>::In, <Self as Errors>::Out>>
    {
        let mut e22 = self.into();
        e22.set_mode(M::MODE).await?;
        Ok(M::from(e22))
    }
}

impl<T: Into + Sized> SwitchMode for T {}
