use embedded_hal::digital::OutputPin;
use embedded_hal_async::digital::Wait;
use embedded_io::{
    asynch::{Read, ReadExactError, Write},
    Io,
};

use crate::Error;

use super::Normal;

impl<SERIAL, IN, OUT> Io for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Error = Error<SERIAL::Error, IN::Error, OUT::Error>;
}

impl<SERIAL, IN, OUT> Read for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        self.0.read(buf).await
    }

    async fn read_exact(&mut self, buf: &mut [u8]) -> Result<(), ReadExactError<Self::Error>> {
        self.0.read_exact(buf).await
    }
}

impl<SERIAL, IN, OUT> Write for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    async fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
        self.0.write(buf).await
    }

    async fn flush(&mut self) -> Result<(), Self::Error> {
        self.0.flush().await
    }
}
