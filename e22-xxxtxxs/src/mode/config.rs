use std::slice;

use embedded_hal::digital::OutputPin;
use embedded_hal_async::digital::Wait;
use embedded_io::asynch;
use futures::{pin_mut, select_biased, FutureExt, TryFutureExt};

use crate::{traits::Errors, Error};

use self::{
    registers::{Add, All, Crypt, Netid, Pid, Reg0, Reg1, Reg2, Reg3},
    traits::{Command, ReadRegister, SetRegister, SetValue, Value},
};

use super::Config;

const SET_REGISTER_COMMAND: u8 = 0xC0;
const READ_REGISTER_COMMAND: u8 = 0xC1;
const SET_TEMPORARY_REGISTER_COMMAND: u8 = 0xC2;

pub mod registers;

mod traits;

pub trait Read: Value {
    async fn add(&mut self) -> Result<Add, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn netid(&mut self) -> Result<Netid, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn reg0(&mut self) -> Result<Reg0, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn reg1(&mut self) -> Result<Reg1, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn reg2(&mut self) -> Result<Reg2, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn reg3(&mut self) -> Result<Reg3, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn all(&mut self) -> Result<All, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }

    async fn pid(&mut self) -> Result<Pid, Error<Self::Serial, Self::In, Self::Out>> {
        self.value().await
    }
}

impl<T: Value> Read for T {}

pub trait Write: SetValue {
    async fn set_add(
        &mut self,
        address: Add,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(address).await
    }

    async fn set_netid(
        &mut self,
        netid: Netid,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(netid).await
    }

    async fn set_reg0(
        &mut self,
        reg0: Reg0,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(reg0).await
    }

    async fn set_reg1(
        &mut self,
        reg1: Reg1,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(reg1).await
    }

    async fn set_reg2(
        &mut self,
        reg2: Reg2,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(reg2).await
    }

    async fn set_reg3(
        &mut self,
        reg3: Reg3,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(reg3).await
    }

    async fn set_crypt(
        &mut self,
        crypt: Crypt,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(crypt).await
    }

    async fn set_all(&mut self, all: All) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        self.set_value(all).await
    }
}

impl<T: SetValue> Write for T {}

pub trait AsPermanent: Command + Sized {
    fn permanent<'a>(&'a mut self) -> Permanent<'a, Self> {
        Permanent(self)
    }
}

pub trait AsWireless: Command + Sized {
    fn wireless<'a>(&'a mut self) -> Wireless<'a, Self> {
        Wireless(self)
    }
}

impl<SERIAL, IN, OUT> Command for Config<SERIAL, IN, OUT>
where
    SERIAL: asynch::Read + asynch::Write,
    IN: Wait,
    OUT: OutputPin,
{
    async fn command(
        &mut self,
        cmd: &[u8],
    ) -> Result<Box<[u8]>, Error<Self::Serial, Self::In, Self::Out>> {
        // TODO Rework this
        self.0.uart.write_all(cmd).map_err(Error::Serial).await?;
        self.0.uart.flush().map_err(Error::Serial).await?;
        // TODO wait for aux low ?

        let mut buf: Vec<u8> = Vec::with_capacity(cmd.len());

        let aux = self.0.aux.wait_for_high().fuse();
        pin_mut!(aux);

        loop {
            buf.reserve(1);
            let ptr = buf.spare_capacity_mut()[0].as_mut_ptr();
            let read = unsafe { slice::from_raw_parts_mut(ptr, 1) };

            select_biased! {
                err = self.0.uart.read(read).fuse() => {
                    err.map_err(Error::Serial)?;
                    unsafe { buf.set_len(buf.len() + 1) };
                },
                err = aux => {
                    err.map_err(Error::InputPin)?;
                    break;
                }
            }
        }

        if buf == Self::WRONG_FORMAT_RESPOND {
            return Err(Error::WrongFormat);
        }

        Ok(buf.into())
    }
}

impl<SERIAL, IN, OUT> ReadRegister for Config<SERIAL, IN, OUT>
where
    SERIAL: asynch::Read + asynch::Write,
    IN: Wait,
    OUT: OutputPin,
{
}

impl<SERIAL, IN, OUT> SetRegister for Config<SERIAL, IN, OUT>
where
    SERIAL: asynch::Read + asynch::Write,
    IN: Wait,
    OUT: OutputPin,
{
    const COMMAND: u8 = SET_TEMPORARY_REGISTER_COMMAND;
}

impl<SERIAL, IN, OUT> AsPermanent for Config<SERIAL, IN, OUT>
where
    SERIAL: asynch::Read + asynch::Write,
    IN: Wait,
    OUT: OutputPin,
{
}

impl<SERIAL, IN, OUT> AsWireless for Config<SERIAL, IN, OUT>
where
    SERIAL: asynch::Read + asynch::Write,
    IN: Wait,
    OUT: OutputPin,
{
}

pub struct Wireless<'a, C: Command>(&'a mut C);

impl<C: Command> Wireless<'_, C> {
    const COMMAND_HEAD: [u8; 2] = [0xCF; 2];
}

impl<C: Command> Errors for Wireless<'_, C> {
    type Serial = C::Serial;
    type In = C::In;
    type Out = C::Out;
}

impl<C: Command> Command for Wireless<'_, C> {
    async fn command(
        &mut self,
        command: &[u8],
    ) -> Result<Box<[u8]>, Error<Self::Serial, Self::In, Self::Out>> {
        let head = &Self::COMMAND_HEAD;
        let cmd = [head, command].concat();
        let buf = self.0.command(&cmd).await?;

        let len = head.len();
        if buf.len() >= len && &buf[..len] == head {
            let buf = &buf[len..];
            // In case errors are not unfolded by the module itself.
            if buf != Self::WRONG_FORMAT_RESPOND {
                Ok(buf.into())
            } else {
                Err(Error::WrongFormat)
            }
        } else {
            Err(Error::Data)
        }
    }
}

impl<C: Command> ReadRegister for Wireless<'_, C> {}

impl<C: Command> SetRegister for Wireless<'_, C> {
    const COMMAND: u8 = SET_TEMPORARY_REGISTER_COMMAND;
}

impl<C: Command> AsPermanent for Wireless<'_, C> {}

pub struct Permanent<'a, C: Command>(&'a mut C);

impl<C: Command> Errors for Permanent<'_, C> {
    type Serial = C::Serial;
    type In = C::In;
    type Out = C::Out;
}

impl<C: Command> Command for Permanent<'_, C> {
    async fn command(
        &mut self,
        command: &[u8],
    ) -> Result<Box<[u8]>, Error<Self::Serial, Self::In, Self::Out>> {
        self.0.command(command).await
    }
}

impl<C: Command> SetRegister for Permanent<'_, C> {}
