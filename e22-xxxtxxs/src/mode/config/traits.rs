use crate::{traits::Errors, Error};

use super::{READ_REGISTER_COMMAND, SET_REGISTER_COMMAND};

pub trait Command: Errors {
    const WRONG_FORMAT_RESPOND: [u8; 3] = [0xFF; 3];

    async fn command(
        &mut self,
        command: &[u8],
    ) -> Result<Box<[u8]>, Error<Self::Serial, Self::In, Self::Out>>;
}

pub trait Register {
    const ADDRESS: u8;
    const LENGTH: usize;
}

pub trait FromBytes: Register {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self;
}

pub trait IntoBytes: Register {
    fn into_bytes(self) -> [u8; Self::LENGTH];
}

pub trait ReadRegister: Command {
    const COMMAND: u8 = READ_REGISTER_COMMAND;

    async fn read_register(
        &mut self,
        address: u8,
        parameter: &mut [u8],
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        let param_len = parameter.len();
        let cmd = [Self::COMMAND, address, param_len as u8];
        let buf = self.command(&cmd).await?;

        let cmd_len = cmd.len();
        if buf.len() == cmd_len + param_len && buf[..cmd_len] == cmd {
            parameter.copy_from_slice(&buf[cmd_len..]);
            Ok(())
        } else {
            Err(Error::Data)
        }
    }
}

pub trait SetRegister: Command {
    const COMMAND: u8 = SET_REGISTER_COMMAND;

    async fn set_register(
        &mut self,
        address: u8,
        parameter: &[u8],
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>> {
        let head = &[Self::COMMAND, address, parameter.len() as u8];
        let mut cmd = [head, parameter].concat();
        let buf = self.command(&cmd).await?;

        cmd[0] = READ_REGISTER_COMMAND;
        if buf[..] == cmd {
            Ok(())
        } else {
            Err(Error::Data)
        }
    }
}

pub trait Value: ReadRegister {
    async fn value<T>(&mut self) -> Result<T, Error<Self::Serial, Self::In, Self::Out>>
    where
        T: Register + FromBytes,
        [u8; T::LENGTH]:, // TODO Remove when const generics are stable
    {
        let mut param = [0; T::LENGTH];
        self.read_register(T::ADDRESS, &mut param).await?;
        Ok(T::from_bytes(param))
    }
}

impl<T: ReadRegister> Value for T {}

pub trait SetValue: SetRegister {
    async fn set_value<T>(
        &mut self,
        value: T,
    ) -> Result<(), Error<Self::Serial, Self::In, Self::Out>>
    where
        T: Register + IntoBytes,
        [u8; T::LENGTH]:, // TODO Remove when const generics are stable
    {
        let param = value.into_bytes();
        self.set_register(T::ADDRESS, &param).await
    }
}

impl<T: SetRegister> SetValue for T {}
