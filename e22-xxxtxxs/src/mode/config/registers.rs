use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::{FromPrimitive, ToPrimitive};

use self::{
    reg0::{AirDataRate, SerialParityBit, SerialPortRate},
    reg1::{RssiAmbientNoise, SubPacket, TransmittingPower},
    reg3::{
        FixedPointTransmission, MonitorBeforeTransmission, Reply, Rssi, WorCycle, WorTransceiver,
    },
};

use super::traits::{FromBytes, IntoBytes, Register};

trait Mask {
    const MASK: u8;
}

trait Value: FromPrimitive + ToPrimitive {
    fn value<T: Mask + FromPrimitive>(&self) -> T {
        let val = self.to_u8().unwrap() & T::MASK;
        T::from_u8(val).unwrap()
    }
}

trait SetValue: FromPrimitive + ToPrimitive {
    fn set_value<T: Mask + ToPrimitive>(&mut self, value: T) {
        let mut val = self.to_u8().unwrap() & !T::MASK;
        val |= value.to_u8().unwrap();
        *self = Self::from_u8(val).unwrap();
    }
}

#[derive(Default)]
pub enum Add {
    #[default]
    Any,
    Address(u16),
}

impl Add {
    pub const ANY: u16 = 0xFFFF; // TODO Check if 0x0000 is also valid

    pub fn new(address: Option<u16>) -> Option<Self> {
        match address {
            Some(addr) => match addr {
                Self::ANY => None,
                addr => Some(Self::Address(addr)),
            },
            None => Some(Self::Any),
        }
    }

    pub fn address(&self) -> Option<u16> {
        match self {
            Self::Any => None,
            Self::Address(addr) => Some(*addr),
        }
    }
}

impl Register for Add {
    const ADDRESS: u8 = 0x00;
    const LENGTH: usize = 2;
}

impl FromBytes for Add {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        match u16::from_be_bytes(bytes) {
            Self::ANY => Self::Any,
            addr => Self::Address(addr),
        }
    }
}

impl IntoBytes for Add {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        match self {
            Self::Any => Self::ANY,
            Self::Address(addr) => addr,
        }
        .to_be_bytes()
    }
}

#[derive(Default)]
pub struct Netid(pub u8);

impl Netid {
    pub fn new(id: u8) -> Self {
        Self(id)
    }

    pub fn id(&self) -> u8 {
        self.0
    }
}

impl Register for Netid {
    const ADDRESS: u8 = 0x02;
    const LENGTH: usize = 1;
}

impl FromBytes for Netid {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(u8::from_be_bytes(bytes))
    }
}

impl IntoBytes for Netid {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

pub mod reg0;

#[derive(FromPrimitive, ToPrimitive)]
pub struct Reg0(u8);

impl Reg0 {
    pub fn new(
        serial_port_rate: SerialPortRate,
        serial_parity_bit: SerialParityBit,
        air_data_rate: AirDataRate,
    ) -> Self {
        Self(serial_port_rate as u8 | serial_parity_bit as u8 | air_data_rate as u8)
    }

    pub fn serial_port_rate(&self) -> SerialPortRate {
        self.value()
    }

    pub fn set_serial_port_rate(&mut self, value: SerialPortRate) {
        self.set_value(value);
    }

    pub fn serial_parity_bit(&self) -> SerialParityBit {
        let val = self.to_u8().unwrap() & SerialParityBit::MASK;
        SerialParityBit::from_u8(val).unwrap_or_default()
    }

    pub fn set_serial_parity_bit(&mut self, value: SerialParityBit) {
        self.set_value(value);
    }

    pub fn air_data_rate(&self) -> AirDataRate {
        self.value()
    }

    pub fn set_air_data_rate(&mut self, value: AirDataRate) {
        self.set_value(value);
    }
}

impl Register for Reg0 {
    const ADDRESS: u8 = 0x03;
    const LENGTH: usize = 1;
}

impl FromBytes for Reg0 {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(u8::from_be_bytes(bytes))
    }
}

impl IntoBytes for Reg0 {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

impl Value for Reg0 {}

impl SetValue for Reg0 {}

impl Default for Reg0 {
    fn default() -> Self {
        Self::new(
            SerialPortRate::default(),
            SerialParityBit::default(),
            AirDataRate::default(),
        )
    }
}

pub mod reg1;

#[derive(FromPrimitive, ToPrimitive)]
pub struct Reg1(u8);

impl Reg1 {
    pub fn new(
        sub_packet: SubPacket,
        rssi_ambient_noise: RssiAmbientNoise,
        transmitting_power: TransmittingPower,
    ) -> Self {
        Self(sub_packet as u8 | rssi_ambient_noise as u8 | transmitting_power as u8)
    }

    pub fn sub_packet(&self) -> SubPacket {
        self.value()
    }

    pub fn set_sub_packet(&mut self, value: SubPacket) {
        self.set_value(value);
    }

    pub fn rssi_ambient_noise(&self) -> RssiAmbientNoise {
        self.value()
    }

    pub fn set_rssi_ambient_noise(&mut self, value: RssiAmbientNoise) {
        self.set_value(value);
    }

    pub fn transmitting_power(&self) -> TransmittingPower {
        self.value()
    }

    pub fn set_transmitting_power(&mut self, value: TransmittingPower) {
        self.set_value(value);
    }
}

impl Register for Reg1 {
    const ADDRESS: u8 = 0x04;
    const LENGTH: usize = 1;
}

impl FromBytes for Reg1 {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(u8::from_be_bytes(bytes))
    }
}

impl IntoBytes for Reg1 {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

impl Value for Reg1 {}

impl SetValue for Reg1 {}

impl Default for Reg1 {
    fn default() -> Self {
        Self::new(
            SubPacket::default(),
            RssiAmbientNoise::default(),
            TransmittingPower::default(),
        )
    }
}

pub mod reg2;

pub struct Reg2(u8);

impl Register for Reg2 {
    const ADDRESS: u8 = 0x05;
    const LENGTH: usize = 1;
}

impl FromBytes for Reg2 {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(u8::from_be_bytes(bytes))
    }
}

impl IntoBytes for Reg2 {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

pub mod reg3;

#[derive(FromPrimitive, ToPrimitive)]
pub struct Reg3(u8);

impl Reg3 {
    pub fn new(
        rssi: Rssi,
        fixed_point_transmission: FixedPointTransmission,
        reply: Reply,
        monitor_before_transmission: MonitorBeforeTransmission,
        wor_transceiver: WorTransceiver,
        wor_cycle: WorCycle,
    ) -> Self {
        Self(
            rssi as u8
                | fixed_point_transmission as u8
                | reply as u8
                | monitor_before_transmission as u8
                | wor_transceiver as u8
                | wor_cycle as u8,
        )
    }

    pub fn rssi(&self) -> Rssi {
        self.value()
    }

    pub fn set_rssi(&mut self, value: Rssi) {
        self.set_value(value);
    }

    pub fn fixed_point_transmission(&self) -> FixedPointTransmission {
        self.value()
    }

    pub fn set_fixed_point_transmission(&mut self, value: FixedPointTransmission) {
        self.set_value(value);
    }

    pub fn reply(&self) -> Reply {
        self.value()
    }

    pub fn set_reply(&mut self, value: Reply) {
        self.set_value(value);
    }

    pub fn monitor_before_transmission(&self) -> MonitorBeforeTransmission {
        self.value()
    }

    pub fn set_monitor_before_transmission(&mut self, value: MonitorBeforeTransmission) {
        self.set_value(value);
    }

    pub fn wor_transceiver(&self) -> WorTransceiver {
        self.value()
    }

    pub fn set_wor_transceiver(&mut self, value: WorTransceiver) {
        self.set_value(value);
    }

    pub fn wor_cycle(&self) -> WorCycle {
        self.value()
    }

    pub fn set_wor_cycle(&mut self, value: WorCycle) {
        self.set_value(value);
    }
}

impl Register for Reg3 {
    const ADDRESS: u8 = 0x06;
    const LENGTH: usize = 1;
}

impl FromBytes for Reg3 {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(u8::from_be_bytes(bytes))
    }
}

impl IntoBytes for Reg3 {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

impl Value for Reg3 {}

impl SetValue for Reg3 {}

impl Default for Reg3 {
    fn default() -> Self {
        Self::new(
            Rssi::default(),
            FixedPointTransmission::default(),
            Reply::default(),
            MonitorBeforeTransmission::default(),
            WorTransceiver::default(),
            WorCycle::default(),
        )
    }
}

#[derive(Default)]
pub struct Crypt(u16);

impl Crypt {
    pub fn new(key: u16) -> Self {
        Self(key)
    }
}

impl Register for Crypt {
    const ADDRESS: u8 = 0x07;
    const LENGTH: usize = 2;
}

impl IntoBytes for Crypt {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0.to_be_bytes()
    }
}

pub struct All([u8; Self::LENGTH]);

impl All {
    pub fn new(
        add: Add,
        netid: Netid,
        reg0: Reg0,
        reg1: Reg1,
        reg2: Reg2,
        reg3: Reg3,
        crypt: Crypt,
    ) -> Self {
        let mut all = Self([0; Self::LENGTH]);
        all.set_add(add);
        all.set_netid(netid);
        all.set_reg0(reg0);
        all.set_reg1(reg1);
        all.set_reg2(reg2);
        all.set_reg3(reg3);
        all.set_crypt(crypt);
        all
    }

    fn read_register(&self, address: u8, parameter: &mut [u8]) {
        let address = address as usize;
        parameter.copy_from_slice(&self.0[address..address + parameter.len()])
    }

    fn set_register(&mut self, address: u8, parameter: &[u8]) {
        let address = address as usize;
        self.0[address..address + parameter.len()].copy_from_slice(parameter)
    }

    fn value<T>(&self) -> T
    where
        T: Register + FromBytes,
        [u8; T::LENGTH]:, // TODO Remove when const generics are stable
    {
        let mut param = [0; T::LENGTH];
        self.read_register(T::ADDRESS, &mut param);
        T::from_bytes(param)
    }

    fn set_value<T>(&mut self, value: T)
    where
        T: Register + IntoBytes,
        [u8; T::LENGTH]:, // TODO Remove when const generics are stable
    {
        let param = value.into_bytes();
        self.set_register(T::ADDRESS, &param)
    }

    pub fn add(&self) -> Add {
        self.value()
    }

    pub fn set_add(&mut self, value: Add) {
        self.set_value(value);
    }

    pub fn netid(&self) -> Netid {
        self.value()
    }

    pub fn set_netid(&mut self, value: Netid) {
        self.set_value(value);
    }

    pub fn reg0(&self) -> Reg0 {
        self.value()
    }

    pub fn set_reg0(&mut self, value: Reg0) {
        self.set_value(value);
    }

    pub fn reg1(&self) -> Reg1 {
        self.value()
    }

    pub fn set_reg1(&mut self, value: Reg1) {
        self.set_value(value);
    }

    pub fn reg2(&self) -> Reg2 {
        self.value()
    }

    pub fn set_reg2(&mut self, value: Reg2) {
        self.set_value(value);
    }

    pub fn reg3(&self) -> Reg3 {
        self.value()
    }

    pub fn set_reg3(&mut self, value: Reg3) {
        self.set_value(value);
    }

    pub fn set_crypt(&mut self, value: Crypt) {
        self.set_value(value);
    }
}

impl Register for All {
    const ADDRESS: u8 = 0x00;
    const LENGTH: usize = Add::LENGTH
        + Netid::LENGTH
        + Reg0::LENGTH
        + Reg1::LENGTH
        + Reg2::LENGTH
        + Reg3::LENGTH
        + Crypt::LENGTH;
}

impl FromBytes for All {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(bytes)
    }
}

impl IntoBytes for All {
    fn into_bytes(self) -> [u8; Self::LENGTH] {
        self.0
    }
}

pub struct Pid(pub [u8; Self::LENGTH]);

impl Pid {
    // It's not clear what those bytes mean. They are not documented in the datasheet.
    // This is my best guess to give them a meaningful use.

    // First byte is called "end" even though it's the first byte. It seems to be always 0x00.
    // So I guess it's a null terminator.

    pub fn features(&self) -> u8 {
        // Maybe some kind of a bit field?
        self.0[1]
    }

    pub fn version(&self) -> (u8, u8) {
        let byte = self.0[2];
        let major = byte >> 4; // 4 most significant bits for major version.
        let minor = byte & 0b0000_1111; // 4 least significant bits for minor version.
        (major, minor) // Not entirely sure but sounds correct.
    }

    pub fn model(&self) -> u8 {
        // Should return `22` as in "E22".
        self.0[3]
    }

    // Remaining bytes are called "reserved". So I'm guessing they are not mapped to anything...
    // And they might be mapped later in newer versions of the chip. But they are not always 0x00.
}

impl Register for Pid {
    const ADDRESS: u8 = 0x80;
    const LENGTH: usize = 7;
}

impl FromBytes for Pid {
    fn from_bytes(bytes: [u8; Self::LENGTH]) -> Self {
        Self(bytes)
    }
}
