use num_derive::{FromPrimitive, ToPrimitive};

use super::Mask;

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum SubPacket {
    #[default]
    Bytes240 = 0b00_0_000_00,
    Bytes128 = 0b01_0_000_00,
    Bytes64 = 0b10_0_000_00,
    Bytes32 = 0b11_0_000_00,
}

impl SubPacket {
    pub fn new(bytes: u8) -> Option<Self> {
        match bytes {
            240 => Some(Self::Bytes240),
            128 => Some(Self::Bytes128),
            64 => Some(Self::Bytes64),
            32 => Some(Self::Bytes32),
            _ => None,
        }
    }

    pub fn bytes(&self) -> u8 {
        match self {
            Self::Bytes240 => 240,
            Self::Bytes128 => 128,
            Self::Bytes64 => 64,
            Self::Bytes32 => 32,
        }
    }
}

impl Mask for SubPacket {
    const MASK: u8 = 0b11_0_000_00;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum RssiAmbientNoise {
    #[default]
    Disable = 0b00_0_000_00,
    Enable = 0b00_1_000_00,
}

impl RssiAmbientNoise {
    pub fn new(enabled: bool) -> Self {
        match enabled {
            false => Self::Disable,
            true => Self::Enable,
        }
    }

    pub fn enabled(&self) -> bool {
        match self {
            Self::Disable => false,
            Self::Enable => true,
        }
    }
}

impl Mask for RssiAmbientNoise {
    const MASK: u8 = 0b00_1_000_00;
}

// Bit 2 is reserved, except for module E22-400T37S that uses it to enable a
// abnomal work status log printing.
// Bits 3 and 4 are reserved.

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum TransmittingPower {
    // TODO Support all modules transmitting power
    #[default]
    Dbm22 = 0b00_0_000_00,
    Dbm17 = 0b00_0_000_01,
    Dbm13 = 0b00_0_000_10,
    Dbm10 = 0b00_0_000_11,
}

impl TransmittingPower {
    pub fn new(dbm: u8) -> Option<Self> {
        match dbm {
            22 => Some(Self::Dbm22),
            17 => Some(Self::Dbm17),
            13 => Some(Self::Dbm13),
            10 => Some(Self::Dbm10),
            _ => None,
        }
    }

    pub fn dbm(&self) -> u8 {
        match self {
            Self::Dbm22 => 22,
            Self::Dbm17 => 17,
            Self::Dbm13 => 13,
            Self::Dbm10 => 10,
        }
    }
}

impl Mask for TransmittingPower {
    const MASK: u8 = 0b00_0_000_11;
}
