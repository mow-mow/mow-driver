use num_derive::{FromPrimitive, ToPrimitive};

use super::Mask;

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum Rssi {
    #[default]
    Disable = 0b0_0_0_0_0_000,
    Enable = 0b1_0_0_0_0_000,
}

impl Rssi {
    pub fn new(enabled: bool) -> Self {
        match enabled {
            false => Self::Disable,
            true => Self::Enable,
        }
    }

    pub fn enabled(&self) -> bool {
        match self {
            Self::Disable => false,
            Self::Enable => true,
        }
    }
}

impl Mask for Rssi {
    const MASK: u8 = 0b1_0_0_0_0_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum FixedPointTransmission {
    #[default]
    Disable = 0b0_0_0_0_0_000,
    Enable = 0b0_1_0_0_0_000,
}

impl FixedPointTransmission {
    pub fn new(enabled: bool) -> Self {
        match enabled {
            false => Self::Disable,
            true => Self::Enable,
        }
    }

    pub fn enabled(&self) -> bool {
        match self {
            Self::Disable => false,
            Self::Enable => true,
        }
    }
}

impl Mask for FixedPointTransmission {
    const MASK: u8 = 0b0_1_0_0_0_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum Reply {
    #[default]
    Disable = 0b0_0_0_0_0_000,
    Enable = 0b0_0_1_0_0_000,
}

impl Reply {
    pub fn new(enabled: bool) -> Self {
        match enabled {
            false => Self::Disable,
            true => Self::Enable,
        }
    }

    pub fn enabled(&self) -> bool {
        match self {
            Self::Disable => false,
            Self::Enable => true,
        }
    }
}

impl Mask for Reply {
    const MASK: u8 = 0b0_0_1_0_0_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum MonitorBeforeTransmission {
    #[default]
    Disable = 0b0_0_0_0_0_000,
    Enable = 0b0_0_0_1_0_000,
}

impl MonitorBeforeTransmission {
    pub fn new(enabled: bool) -> Self {
        match enabled {
            false => Self::Disable,
            true => Self::Enable,
        }
    }

    pub fn enabled(&self) -> bool {
        match self {
            Self::Disable => false,
            Self::Enable => true,
        }
    }
}

impl Mask for MonitorBeforeTransmission {
    const MASK: u8 = 0b0_0_0_1_0_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum WorTransceiver {
    #[default]
    Receiver = 0b0_0_0_0_0_000,
    Transmitter = 0b0_0_0_0_1_000,
}

impl WorTransceiver {
    pub fn new(transmitter: bool) -> Self {
        match transmitter {
            false => Self::Receiver,
            true => Self::Transmitter,
        }
    }

    pub fn receiver(&self) -> bool {
        match self {
            Self::Receiver => true,
            Self::Transmitter => false,
        }
    }

    pub fn transmitter(&self) -> bool {
        match self {
            Self::Receiver => false,
            Self::Transmitter => true,
        }
    }
}

impl Mask for WorTransceiver {
    const MASK: u8 = 0b0_0_0_0_1_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum WorCycle {
    Ms500 = 0b0_0_0_0_0_000,
    Ms1000 = 0b0_0_0_0_0_001,
    Ms1500 = 0b0_0_0_0_0_010,
    #[default]
    Ms2000 = 0b0_0_0_0_0_011,
    Ms2500 = 0b0_0_0_0_0_100,
    Ms3000 = 0b0_0_0_0_0_101,
    Ms3500 = 0b0_0_0_0_0_110,
    Ms4000 = 0b0_0_0_0_0_111,
}

impl WorCycle {
    pub fn new(ms: u16) -> Option<Self> {
        match ms {
            500 => Some(Self::Ms500),
            1000 => Some(Self::Ms1000),
            1500 => Some(Self::Ms1500),
            2000 => Some(Self::Ms2000),
            2500 => Some(Self::Ms2500),
            3000 => Some(Self::Ms3000),
            3500 => Some(Self::Ms3500),
            4000 => Some(Self::Ms4000),
            _ => None,
        }
    }

    pub fn ms(&self) -> u16 {
        match self {
            Self::Ms500 => 500,
            Self::Ms1000 => 1000,
            Self::Ms1500 => 1500,
            Self::Ms2000 => 2000,
            Self::Ms2500 => 2500,
            Self::Ms3000 => 3000,
            Self::Ms3500 => 3500,
            Self::Ms4000 => 4000,
        }
    }
}

impl Mask for WorCycle {
    const MASK: u8 = 0b0_0_0_0_0_111;
}
