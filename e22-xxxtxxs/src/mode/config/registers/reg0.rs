use num_derive::{FromPrimitive, ToPrimitive};

use super::Mask;

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum SerialPortRate {
    Bps1200 = 0b000_00_000,
    Bps2400 = 0b001_00_000,
    Bps4800 = 0b010_00_000,
    #[default]
    Bps9600 = 0b011_00_000,
    Bps19200 = 0b100_00_000,
    Bps38400 = 0b101_00_000,
    Bps57600 = 0b110_00_000,
    Bps115200 = 0b111_00_000,
}

impl SerialPortRate {
    pub fn new(bps: u32) -> Option<Self> {
        match bps {
            1200 => Some(Self::Bps1200),
            2400 => Some(Self::Bps2400),
            4800 => Some(Self::Bps4800),
            9600 => Some(Self::Bps9600),
            19200 => Some(Self::Bps19200),
            38400 => Some(Self::Bps38400),
            57600 => Some(Self::Bps57600),
            115200 => Some(Self::Bps115200),
            _ => None,
        }
    }

    pub fn bps(&self) -> u32 {
        match self {
            Self::Bps1200 => 1200,
            Self::Bps2400 => 2400,
            Self::Bps4800 => 4800,
            Self::Bps9600 => 9600,
            Self::Bps19200 => 19200,
            Self::Bps38400 => 38400,
            Self::Bps57600 => 57600,
            Self::Bps115200 => 115200,
        }
    }
}

impl Mask for SerialPortRate {
    const MASK: u8 = 0b111_00_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum SerialParityBit {
    #[default]
    None = 0b000_00_000,
    Odd = 0b000_01_000,
    Even = 0b000_10_000,
}

impl Mask for SerialParityBit {
    const MASK: u8 = 0b000_11_000;
}

#[derive(Default, FromPrimitive, ToPrimitive)]
pub enum AirDataRate {
    // According to the datasheet, module E22-900T22S doesn't support the two
    // first rates.
    // But I think it's a mistake in the datasheet.
    K0_3 = 0b000_00_000,
    K1_2 = 0b000_00_001,
    #[default]
    K2_4 = 0b000_00_010,
    K4_8 = 0b000_00_011,
    K9_6 = 0b000_00_100,
    K19_2 = 0b000_00_101,
    K38_4 = 0b000_00_110,
    K62_5 = 0b000_00_111,
}

impl AirDataRate {
    pub fn new(rate: u16) -> Option<Self> {
        match rate {
            300 => Some(Self::K0_3),
            1200 => Some(Self::K1_2),
            2400 => Some(Self::K2_4),
            4800 => Some(Self::K4_8),
            9600 => Some(Self::K9_6),
            19200 => Some(Self::K19_2),
            38400 => Some(Self::K38_4),
            62500 => Some(Self::K62_5),
            _ => None,
        }
    }

    pub fn rate(&self) -> u16 {
        match self {
            Self::K0_3 => 300,
            Self::K1_2 => 1200,
            Self::K2_4 => 2400,
            Self::K4_8 => 4800,
            Self::K9_6 => 9600,
            Self::K19_2 => 19200,
            Self::K38_4 => 38400,
            Self::K62_5 => 62500,
        }
    }
}

impl Mask for AirDataRate {
    const MASK: u8 = 0b000_00_111;
}
