#![allow(incomplete_features)]
#![feature(async_fn_in_trait)]
#![feature(generic_const_exprs)]
#![feature(impl_trait_projections)]

use embedded_hal::digital::{self, OutputPin, PinState};
use embedded_hal_async::digital::Wait;
use embedded_io::{
    asynch::{Read, Write},
    blocking::ReadExactError,
    ErrorKind,
};
use futures::TryFutureExt;
use traits::{From, Into, Types};

mod traits;

pub mod mode;

#[derive(Debug)]
pub enum Error<SERIAL, IN, OUT>
where
    SERIAL: embedded_io::Error,
    IN: digital::Error,
    OUT: digital::Error,
{
    Serial(SERIAL),
    InputPin(IN),
    OutputPin(OUT),
    WrongFormat,
    Data,
}

impl<SERIAL, IN, OUT> embedded_io::Error for Error<SERIAL, IN, OUT>
where
    SERIAL: embedded_io::Error,
    IN: digital::Error,
    OUT: digital::Error,
{
    fn kind(&self) -> embedded_io::ErrorKind {
        ErrorKind::Other
    }
}

pub struct E22xxxtxxs<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    uart: SERIAL,
    aux: IN,
    mode: (OUT, OUT),
}

impl<SERIAL, IN, OUT> E22xxxtxxs<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    const MAX_BUF_SIZE: usize = 1000;

    pub fn new(uart: SERIAL, aux: IN, mode: (OUT, OUT)) -> Self {
        Self { uart, aux, mode }
    }

    async fn set_mode(
        &mut self,
        mode: (PinState, PinState),
    ) -> Result<(), Error<SERIAL::Error, IN::Error, OUT::Error>> {
        self.mode.0.set_state(mode.0).map_err(Error::OutputPin)?;
        self.mode.1.set_state(mode.1).map_err(Error::OutputPin)?;
        // FIXME wait for aux low?
        // We might also use wait_for_rising_edge
        self.aux.wait_for_high().map_err(Error::InputPin).await
    }

    async fn read(
        &mut self,
        buf: &mut [u8],
    ) -> Result<usize, Error<SERIAL::Error, IN::Error, OUT::Error>> {
        self.aux.wait_for_high().map_err(Error::InputPin).await?; // Ensure that all data has been recieved
        self.uart.read(buf).map_err(Error::Serial).await
    }

    async fn read_exact(
        &mut self,
        mut buf: &mut [u8],
    ) -> Result<(), ReadExactError<Error<SERIAL::Error, IN::Error, OUT::Error>>> {
        let mut len = self.read(buf).map_err(ReadExactError::Other).await?;
        loop {
            buf = &mut buf[len..];
            if buf.is_empty() {
                return Ok(());
            }
            if len == 0 {
                return Err(ReadExactError::UnexpectedEof);
            }
            len = self
                .aux
                .wait_for_rising_edge()
                .map_err(Error::InputPin)
                .and_then(|()| self.uart.read(buf).map_err(Error::Serial))
                .map_err(ReadExactError::Other)
                .await?;
        }
    }

    async fn write(
        &mut self,
        buf: &[u8],
    ) -> Result<usize, Error<SERIAL::Error, IN::Error, OUT::Error>> {
        self.flush().await?;
        let len = Self::MAX_BUF_SIZE.min(buf.len());
        self.uart.write(&buf[..len]).map_err(Error::Serial).await
    }

    async fn write_all(
        &mut self,
        mut buf: &[u8],
    ) -> Result<(), Error<SERIAL::Error, IN::Error, OUT::Error>> {
        loop {
            self.flush().await?;
            let len = Self::MAX_BUF_SIZE.min(buf.len());
            self.uart
                .write_all(&buf[..len])
                .map_err(Error::Serial)
                .await?;
            buf = &buf[len..];
            if buf.is_empty() {
                return Ok(());
            }
        }
    }

    async fn flush(&mut self) -> Result<(), Error<SERIAL::Error, IN::Error, OUT::Error>> {
        self.uart.flush().map_err(Error::Serial).await?;
        self.aux.wait_for_high().map_err(Error::InputPin).await // Ensure that all data has been sent
    }
}

impl<SERIAL, IN, OUT> Types for E22xxxtxxs<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Serial = SERIAL;
    type In = IN;
    type Out = OUT;
}

impl<SERIAL, IN, OUT> From for E22xxxtxxs<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self {
        e22
    }
}

impl<SERIAL, IN, OUT> Into for E22xxxtxxs<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out> {
        self
    }
}
