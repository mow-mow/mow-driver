use embedded_hal::digital::{OutputPin, PinState};
use embedded_hal_async::digital::Wait;
use embedded_io::asynch::{Read, Write};

use crate::{
    traits::{Errors, From, Into, Mode, SwitchMode, Types},
    E22xxxtxxs, Error,
};

pub trait Switch: SwitchMode {
    async fn normal(
        self,
    ) -> Result<
        Normal<Self::Serial, Self::In, Self::Out>,
        Error<<Self as Errors>::Serial, <Self as Errors>::In, <Self as Errors>::Out>,
    > {
        self.switch_mode().await
    }

    async fn wor(
        self,
    ) -> Result<
        Wor<Self::Serial, Self::In, Self::Out>,
        Error<<Self as Errors>::Serial, <Self as Errors>::In, <Self as Errors>::Out>,
    > {
        self.switch_mode().await
    }

    async fn config(
        self,
    ) -> Result<
        Config<Self::Serial, Self::In, Self::Out>,
        Error<<Self as Errors>::Serial, <Self as Errors>::In, <Self as Errors>::Out>,
    > {
        self.switch_mode().await
    }

    async fn deep_sleep(
        self,
    ) -> Result<
        DeepSleep<Self::Serial, Self::In, Self::Out>,
        Error<<Self as Errors>::Serial, <Self as Errors>::In, <Self as Errors>::Out>,
    > {
        self.switch_mode().await
    }
}

impl<T: SwitchMode> Switch for T {}

pub mod normal;

pub struct Normal<SERIAL, IN, OUT>(E22xxxtxxs<SERIAL, IN, OUT>)
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin;

impl<SERIAL, IN, OUT> Mode for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    const MODE: (PinState, PinState) = (PinState::Low, PinState::Low);
}

impl<SERIAL, IN, OUT> Types for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Serial = SERIAL;
    type In = IN;
    type Out = OUT;
}

impl<SERIAL, IN, OUT> From for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self {
        Self(e22)
    }
}

impl<SERIAL, IN, OUT> Into for Normal<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out> {
        self.0
    }
}

pub mod wor;

pub struct Wor<SERIAL, IN, OUT>(E22xxxtxxs<SERIAL, IN, OUT>)
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin;

impl<SERIAL, IN, OUT> Mode for Wor<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    const MODE: (PinState, PinState) = (PinState::High, PinState::Low);
}

impl<SERIAL, IN, OUT> Types for Wor<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Serial = SERIAL;
    type In = IN;
    type Out = OUT;
}

impl<SERIAL, IN, OUT> From for Wor<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self {
        Self(e22)
    }
}

impl<SERIAL, IN, OUT> Into for Wor<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out> {
        self.0
    }
}

pub mod config;

pub struct Config<SERIAL, IN, OUT>(E22xxxtxxs<SERIAL, IN, OUT>)
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin;

impl<SERIAL, IN, OUT> Mode for Config<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    const MODE: (PinState, PinState) = (PinState::Low, PinState::High);
}

impl<SERIAL, IN, OUT> Types for Config<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Serial = SERIAL;
    type In = IN;
    type Out = OUT;
}

impl<SERIAL, IN, OUT> From for Config<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self {
        Self(e22)
    }
}

impl<SERIAL, IN, OUT> Into for Config<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out> {
        self.0
    }
}

pub mod deep_sleep;

pub struct DeepSleep<SERIAL, IN, OUT>(E22xxxtxxs<SERIAL, IN, OUT>)
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin;

impl<SERIAL, IN, OUT> Mode for DeepSleep<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    const MODE: (PinState, PinState) = (PinState::High, PinState::High);
}

impl<SERIAL, IN, OUT> Types for DeepSleep<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    type Serial = SERIAL;
    type In = IN;
    type Out = OUT;
}

impl<SERIAL, IN, OUT> From for DeepSleep<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn from(e22: E22xxxtxxs<Self::Serial, Self::In, Self::Out>) -> Self {
        Self(e22)
    }
}

impl<SERIAL, IN, OUT> Into for DeepSleep<SERIAL, IN, OUT>
where
    SERIAL: Read + Write,
    IN: Wait,
    OUT: OutputPin,
{
    fn into(self) -> E22xxxtxxs<Self::Serial, Self::In, Self::Out> {
        self.0
    }
}
