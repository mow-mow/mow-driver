pub mod i2c;
pub mod serial;

// TODO Use embedded_io traits for UbloxInterface

#[non_exhaustive]
pub enum ErrorKind {
    Data,
    Empty,
    Overrun,
    Protocol,
    Control,
    Other,
}

pub trait Error {
    fn kind(&self) -> ErrorKind;
}

pub trait UbloxInterface {
    type Error: Error;

    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error>;

    async fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error>;

    async fn flush(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}
