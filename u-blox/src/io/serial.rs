use std::{
    error,
    fmt::{self, Debug, Display, Formatter},
};

use embedded_io::asynch::{Read, Write};
use futures::TryFutureExt;

use super::UbloxInterface;

#[derive(Debug)]
pub struct UbloxSerialInterfaceError<SERIAL>(SERIAL)
where
    SERIAL: embedded_io::Error;

impl<SERIAL> super::Error for UbloxSerialInterfaceError<SERIAL>
where
    SERIAL: embedded_io::Error,
{
    fn kind(&self) -> super::ErrorKind {
        super::ErrorKind::Other
    }
}

impl<SERIAL> Display for UbloxSerialInterfaceError<SERIAL>
where
    SERIAL: embedded_io::Error + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Serial error: {}", self.0)
    }
}

impl<SERIAL> error::Error for UbloxSerialInterfaceError<SERIAL>
where
    SERIAL: embedded_io::Error + error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

pub struct UbloxSerialInterface<SERIAL>(SERIAL)
where
    SERIAL: Read + Write;

impl<SERIAL> UbloxSerialInterface<SERIAL>
where
    SERIAL: Read + Write,
{
    pub fn new(serial: SERIAL) -> Self {
        Self(serial)
    }
}

impl<SERIAL> UbloxInterface for UbloxSerialInterface<SERIAL>
where
    SERIAL: Read + Write,
{
    type Error = UbloxSerialInterfaceError<SERIAL::Error>;

    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        self.0.read(buf).map_err(UbloxSerialInterfaceError).await
    }

    async fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.0
            .write_all(buf)
            .map_err(UbloxSerialInterfaceError)
            .await
    }

    async fn flush(&mut self) -> Result<(), Self::Error> {
        self.0.flush().map_err(UbloxSerialInterfaceError).await
    }
}
