use std::{
    error,
    fmt::{self, Debug, Display, Formatter},
};

use embedded_hal::digital::{self, PinState};
use embedded_hal_async::{
    delay::DelayUs,
    digital::Wait,
    i2c::{self, I2c, NoAcknowledgeSource, SevenBitAddress},
};
use futures::TryFutureExt;

use super::UbloxInterface;

#[derive(Debug)]
pub enum UbloxI2cError<I2C>
where
    I2C: i2c::Error,
{
    I2c(I2C),
    Protocol,
}

impl<I2C> super::Error for UbloxI2cError<I2C>
where
    I2C: i2c::Error,
{
    fn kind(&self) -> super::ErrorKind {
        match self {
            Self::I2c(err) => match err.kind() {
                i2c::ErrorKind::Bus => super::ErrorKind::Data,
                i2c::ErrorKind::NoAcknowledge(err) => match err {
                    NoAcknowledgeSource::Data => super::ErrorKind::Data,
                    NoAcknowledgeSource::Unknown => super::ErrorKind::Data,
                    NoAcknowledgeSource::Address => super::ErrorKind::Empty,
                },
                i2c::ErrorKind::Overrun => super::ErrorKind::Overrun,
                i2c::ErrorKind::ArbitrationLoss => super::ErrorKind::Control,
                _ => super::ErrorKind::Other,
            },
            Self::Protocol => super::ErrorKind::Protocol,
        }
    }
}

impl<I2C> Display for UbloxI2cError<I2C>
where
    I2C: i2c::Error + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::I2c(err) => write!(f, "I2C error: {}", err),
            Self::Protocol => write!(f, "Protocol error"),
        }
    }
}

impl<I2C> error::Error for UbloxI2cError<I2C>
where
    I2C: i2c::Error + error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::I2c(err) => Some(err),
            Self::Protocol => None,
        }
    }
}

pub struct UbloxI2c<I2C>
where
    I2C: I2c,
{
    i2c: I2C,
    addr: SevenBitAddress, // FIXME Is it really 7-bit?
}

impl<I2C> UbloxI2c<I2C>
where
    I2C: I2c,
{
    pub fn new(i2c: I2C, addr: SevenBitAddress) -> Self {
        Self { i2c, addr }
    }

    const AVAIL_ADDR: u8 = 0xFD;

    async fn available(&mut self) -> Result<usize, UbloxI2cError<I2C::Error>> {
        let mut read = [0; 2];
        self.i2c
            .write_read(self.addr, &[Self::AVAIL_ADDR], &mut read)
            .map_err(UbloxI2cError::I2c)
            .await?;
        Ok(u16::from_be_bytes(read) as usize)
    }

    const READ_ADDR: u8 = 0xFF;

    async fn read(&mut self, buf: &mut [u8]) -> Result<(), UbloxI2cError<I2C::Error>> {
        self.i2c
            .write_read(self.addr, &[Self::READ_ADDR], buf)
            .map_err(UbloxI2cError::I2c)
            .await
    }

    const MIN_BUF_SIZE: usize = 2;

    async fn write(&mut self, buf: &[u8]) -> Result<(), UbloxI2cError<I2C::Error>> {
        if buf.len() < Self::MIN_BUF_SIZE {
            return Err(UbloxI2cError::Protocol);
        }
        self.i2c
            .write(self.addr, buf)
            .map_err(UbloxI2cError::I2c)
            .await
    }

    pub async fn wakeup(&mut self) -> Result<(), UbloxI2cError<I2C::Error>> {
        self.available().await?;
        Ok(())
    }
}

#[derive(Debug)]
pub enum UbloxInterruptedInterfaceError<I2C, PIN>
where
    I2C: i2c::Error,
    PIN: digital::Error,
{
    I2c(UbloxI2cError<I2C>),
    Pin(PIN),
}

impl<I2C, PIN> super::Error for UbloxInterruptedInterfaceError<I2C, PIN>
where
    I2C: i2c::Error,
    PIN: digital::Error,
{
    fn kind(&self) -> super::ErrorKind {
        match self {
            Self::I2c(err) => err.kind(),
            Self::Pin(_) => super::ErrorKind::Other,
        }
    }
}

impl<I2C, PIN> Display for UbloxInterruptedInterfaceError<I2C, PIN>
where
    I2C: i2c::Error + Display,
    PIN: digital::Error + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::I2c(err) => write!(f, "I2C error: {}", err),
            Self::Pin(err) => write!(f, "Pin error: {}", err),
        }
    }
}

impl<I2C, PIN> error::Error for UbloxInterruptedInterfaceError<I2C, PIN>
where
    I2C: i2c::Error + error::Error + 'static,
    PIN: digital::Error + error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::I2c(err) => Some(err),
            Self::Pin(err) => Some(err),
        }
    }
}

pub struct UbloxInterruptedInterface<I2C, PIN>
where
    I2C: I2c,
    PIN: Wait,
{
    i2c: UbloxI2c<I2C>,
    int: PIN,
    polarity: PinState,
}

impl<I2C, PIN> UbloxInterruptedInterface<I2C, PIN>
where
    I2C: I2c,
    PIN: Wait,
{
    pub fn new(i2c: UbloxI2c<I2C>, int: PIN, polarity: PinState) -> Self {
        Self { i2c, int, polarity }
    }
}

impl<I2C, PIN> UbloxInterface for UbloxInterruptedInterface<I2C, PIN>
where
    I2C: I2c,
    PIN: Wait,
{
    type Error = UbloxInterruptedInterfaceError<I2C::Error, PIN::Error>;

    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        let mut len;
        loop {
            match self.polarity {
                PinState::High => self.int.wait_for_high().await,
                PinState::Low => self.int.wait_for_low().await,
            }
            .map_err(UbloxInterruptedInterfaceError::Pin)?;
            len = self
                .i2c
                .available()
                .map_err(UbloxInterruptedInterfaceError::I2c)
                .await?;
            if len > 0 {
                break;
            }
        }
        len = len.min(buf.len());
        self.i2c
            .read(&mut buf[..len])
            .map_err(UbloxInterruptedInterfaceError::I2c)
            .await?;
        Ok(len)
    }

    async fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.i2c
            .write(buf)
            .map_err(UbloxInterruptedInterfaceError::I2c)
            .await
    }
}

#[derive(Debug)]
pub enum UbloxPolledInterfaceError<I2C>
where
    I2C: i2c::Error,
{
    I2c(UbloxI2cError<I2C>),
}

impl<I2C> super::Error for UbloxPolledInterfaceError<I2C>
where
    I2C: i2c::Error,
{
    fn kind(&self) -> super::ErrorKind {
        match self {
            Self::I2c(err) => err.kind(),
        }
    }
}

impl<I2C> Display for UbloxPolledInterfaceError<I2C>
where
    I2C: i2c::Error + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::I2c(err) => write!(f, "I2C error: {}", err),
        }
    }
}

impl<I2C> error::Error for UbloxPolledInterfaceError<I2C>
where
    I2C: i2c::Error + error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::I2c(err) => Some(err),
        }
    }
}

pub struct UbloxPolledInterface<I2C, DELAY>
where
    I2C: I2c,
    DELAY: DelayUs,
{
    i2c: UbloxI2c<I2C>,
    delay: DELAY,
    ms: u32,
}

impl<I2C, DELAY> UbloxPolledInterface<I2C, DELAY>
where
    I2C: I2c,
    DELAY: DelayUs,
{
    pub fn new(i2c: UbloxI2c<I2C>, delay: DELAY, ms: u32) -> Self {
        Self { i2c, delay, ms }
    }
}

impl<I2C, DELAY> UbloxInterface for UbloxPolledInterface<I2C, DELAY>
where
    I2C: I2c,
    DELAY: DelayUs,
{
    type Error = UbloxPolledInterfaceError<I2C::Error>;

    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        let mut len;
        loop {
            len = self
                .i2c
                .available()
                .map_err(UbloxPolledInterfaceError::I2c)
                .await?;
            if len > 0 {
                break;
            }
            self.delay.delay_ms(self.ms).await;
        }
        len = len.min(buf.len());
        self.i2c
            .read(&mut buf[..len])
            .map_err(UbloxPolledInterfaceError::I2c)
            .await?;
        Ok(len)
    }

    async fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.i2c
            .write(buf)
            .map_err(UbloxPolledInterfaceError::I2c)
            .await
    }
}
